package com.atlassian.bamboo.slack;

import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.notification.recipients.AbstractNotificationRecipient;
import com.atlassian.bamboo.deployments.notification.DeploymentResultAwareNotificationRecipient;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plugin.descriptor.NotificationRecipientModuleDescriptor;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public class SlackNotificationRecipient extends AbstractNotificationRecipient implements DeploymentResultAwareNotificationRecipient,
                                                                                           NotificationRecipient.RequiresPlan,
                                                                                           NotificationRecipient.RequiresResultSummary

{
    private static final Logger log = Logger.getLogger(SlackNotificationRecipient.class);
    private static String DOMAIN = "domain";
    private static String API_TOKEN = "apiToken";
    private static String ROOM = "room";
    private static String ICON_URL = "iconUrl";

    private String domain = null;
    private String apiToken = null;
    private String room = null;
    private String iconUrl = null;

    private TemplateRenderer templateRenderer;

    private ImmutablePlan plan;
    private ResultsSummary resultsSummary;
    private DeploymentResult deploymentResult;
    private CustomVariableContext customVariableContext;

    @Override
    public void populate(@NotNull Map<String, String[]> params)
    {
        for (String next : params.keySet())
        {
            System.out.println("next = " + next);
        }
        if (params.containsKey(DOMAIN))
        {
            this.domain = params.get(DOMAIN)[1];
        }
        if (params.containsKey(API_TOKEN))
        {
            this.apiToken = params.get(API_TOKEN)[1];
        }
        if (params.containsKey(ROOM))
        {
            this.room = params.get(ROOM)[1];
        }
        if (params.containsKey(ICON_URL)) {
            this.iconUrl = params.get(ICON_URL)[1];
        }
    }

    @Override
    public void init(@Nullable String configurationData)
    {

        if (StringUtils.isNotBlank(configurationData) && configurationData.contains("|"))
        {
            String delimiter = "\\|";

            String[] configValues = configurationData.split(delimiter);

                if (configValues.length > 0) {
                    domain = configValues[0];
                }
                if (configValues.length > 1) {
                    apiToken = configValues[1];
                }
                if (configValues.length > 2) {
                    room = configValues[2];
                }
                if (configValues.length > 3) {
                    iconUrl = configValues[3];
                }
        }
    }

    @NotNull
    @Override
    public String getRecipientConfig()
    {
        // We can do this because API tokens don't have | in them, but it's pretty dodge. Better to JSONify or something?
        String delimiter = "|";

        StringBuilder recipientConfig = new StringBuilder(domain + delimiter + apiToken + delimiter + room);

        if (StringUtils.isNotBlank(iconUrl)) {
            recipientConfig.append(delimiter);
            recipientConfig.append(iconUrl);
        }
        return recipientConfig.toString();
    }

    @NotNull
    @Override
    public String getEditHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getEditTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }

    private Map<String, Object> populateContext()
    {
        Map<String, Object> context = Maps.newHashMap();
        if (domain != null)
        {
            context.put(DOMAIN, domain);
        }
        if (apiToken != null)
        {
            context.put(API_TOKEN, apiToken);
        }
        if (room != null)
        {
            context.put(ROOM, room);
        }

        if (iconUrl != null) {
            context.put(ICON_URL, iconUrl);
        }

        System.out.println("populateContext = " + context.toString());

        return context;
    }

    @NotNull
    @Override
    public String getViewHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getViewTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }



    @NotNull
    @Override
    public List<NotificationTransport> getTransports()
    {
        List<NotificationTransport> list = Lists.newArrayList();
        list.add(new SlackNotificationTransport(domain, apiToken, room, iconUrl, plan, resultsSummary, deploymentResult, customVariableContext));
        return list;
    }

    public void setPlan(@Nullable final Plan plan)
    {
        this.plan = plan;
    }

    @Override
    public void setPlan(@Nullable final ImmutablePlan plan)
    {
        this.plan = plan;
    }

    @Override
    public void setDeploymentResult(@Nullable final DeploymentResult deploymentResult)
    {
        this.deploymentResult = deploymentResult;
    }

    @Override
    public void setResultsSummary(@Nullable final ResultsSummary resultsSummary)
    {
        this.resultsSummary = resultsSummary;
    }

    //-----------------------------------Dependencies
    public void setTemplateRenderer(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    public void setCustomVariableContext(CustomVariableContext customVariableContext) { this.customVariableContext = customVariableContext; }
}

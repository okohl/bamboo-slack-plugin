[@ww.textfield labelKey="slack.domain" name="domain" value="${domain!}" required='true'/]
[@ww.textfield labelKey="slack.api.token" name="apiToken" value="${apiToken!}" required='true'/]
[@ww.textfield labelKey="slack.room" name="room" value="${room!}" required='true'/]
[@ww.textfield labelKey="slack.iconUrl" name="iconUrl" value="${iconUrl!}" required='false'/]

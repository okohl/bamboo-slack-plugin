package com.atlassian.bamboo.slack;


import junit.framework.Assert;
import org.junit.Test;

public class SlackMessageParsingTest {

    private final String INPUT = "<img src='http://SOP-MARACHE.local:6990/bamboo/images/iconsv4/icon-build-successful.png' height='16' width='16' align='absmiddle' />&nbsp;<a href='http://SOP-MARACHE.local:6990/bamboo/browse/TEST-TEST-8'>test &rsaquo; test &rsaquo; #8</a> passed. Manual run by <a href=\"http://SOP-MARACHE.local:6990/bamboo/browse/user/admin\">admin</a>";
    private final String OUTPUT_FALLBACK = "test › test › #8 passed. Manual run by admin";
    private final String OUTPUT_TEXT = "<http://SOP-MARACHE.local:6990/bamboo/browse/TEST-TEST-8|test › test › #8> passed. Manual run by <http://SOP-MARACHE.local:6990/bamboo/browse/user/admin|admin>";

    @Test
    public void testFallbackMessage()
    {
        String output = SlackNotificationTransport.fallbackMessage(INPUT);
        Assert.assertEquals(OUTPUT_FALLBACK,output);
    }

    @Test
    public void testTextMessage()
    {
        String output = SlackNotificationTransport.textMessage(INPUT);
        Assert.assertEquals(OUTPUT_TEXT,output);
    }
}
